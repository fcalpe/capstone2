

let adminUser = localStorage.getItem("isAdmin");

/*let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let email = document.querySelector("#email");*/

console.log(adminUser);

let token = localStorage.getItem('token');

//fetch user details
fetch('https://arcane-wildwood-05733.herokuapp.com/api/users/details', {
	method: 'GET',
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	console.log(data);

	let userData;

	//Get the enrolled courses
	userData = data.enrollments.map(user => {

	fetch('https://arcane-wildwood-05733.herokuapp.com/api/courses/:courseId', {
		method: 'GET'
	})
	.then(res => res.json())
	.then(data => {

		console.log(course._id);

		//without courses
		if(data.enrollments === 0){

			cardUser =
			`
			<ul>
				<li>Name: ${data.firstName}</li>
				<li>Last Name: ${data.lastName}</li>
				<li>Email: ${data.email}</li>
			</ul>
			<p>"You are not enrolled in anything yet</p>
			`

		//with Courses
		} else {

			cardUser =
			`
			<ul>
				<li>Name: ${data.firstName}</li>
				<li>Last Name: ${data.lastName}</li>
				<li>Email: ${data.email}</li>
			</ul>
			`
		}

		//code reflected on the page

		return (

			`
			<div class='card'>
				<div class='card-body'>
					${cardUSer}
				</div>
				<div class='card-table'>
					<th>Enrolled Courses</th>
					<th>Date Enrolled</th>
					<th>Enrollment Status</th>
					<tr>
					<td>Course Name: ${course.name}</td>
					<td>Date: ${course.date}</td>
					<td>Status: ${course.status}</td>
					</tr>
				</div>
			</div>
			`
		)

		let profileContainer = document.querySelector("#profileContainer");

		profileContainer.innerHTML = userData;

	})
